<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Repository\Contracts\UserRepositoryInterface;

class InvitationController extends Controller
{
    private $userRepo;
    /**
     * Create a new controller instance.
     * @inject UserRepositoryInterface
     * @return void
     */
    public function __construct(
        UserRepositoryInterface $userRepo
    )
    {
        $this->middleware('auth');
        $this->userRepo = $userRepo;
    }

    public function sendInvitation(Request $request) {
        try {
            // Getting form data from request
            $inputData = $request->input();

            // Validate form data
            $validator = Validator::make($inputData, [
                'email' => ['required', 'string', 'email', 'max:255']
            ]);

            // Return back if form data is invalid
            if ($validator->fails())
            {
                return Redirect::to('invite')->withErrors($validator);
            }

            // call repository function to send invitation
            $response = $this->userRepo->inviteUser(
                $inputData['email'],
                Auth::user()
            );

            // set invitation status flash message to show in view
            $request->session()->flash('invitation_status', $response);
            return Redirect::to('invite');

        } catch (\Exception $ex) {
            return "Somthing went wrong!";
        }
    }

    public function acceptInvitation($invitation_code, Request $request) {
        try {
            // Accepting invitation by invitation code and current user
            $acceptStatus = $this->userRepo->acceptInvitation($invitation_code, Auth::user()->id);

            // checking status whether true or false
            if (!$acceptStatus) {
                $response = [
                    'status' => false,
                    'message' => 'Invalid Invitation!'
                ];
            } else {
                $response = [
                    'status' => true,
                    'message' => 'You accepted the invitation!'
                ];
            }

            // Set status flash message to show in view
            $request->session()->flash('accept_status', $response);
            return Redirect::to('home');

        } catch(\Exception $ex) {
            return "something went wrong!";
        }
    }
}
