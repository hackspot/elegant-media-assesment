<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\Contracts\UserRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    private $userRepo;
    /**
     * Create a new controller instance.
     * @inject UserRepositoryInterface
     * @return void
     */
    public function __construct(
        UserRepositoryInterface $userRepo
    )
    {
        $this->middleware('auth');
        $this->userRepo = $userRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function friendsListView(Request $request)
    {
        try {
            // Get current logged-in user
            $currentUser = Auth::user();

            // Get Search query
            $searchKey = ($request->input('name')) ? $request->input('name') : '';

            // Get friends list of current logged-in user
            $friendsList = $this->userRepo->getFriendsListByUserId($currentUser->id, $searchKey);

            return view('home',[
                "tab" => "friendsList",
                "friendsList" => $friendsList,
            ]);
        } catch (\Exception $ex) {
            return "Something Went Wrong!";
        }
    }

    public function inviteView()
    {
        try {
            return view('home',[
                "tab" => "invite",
            ]);
        } catch (\Exception $ex) {
            return "Something Went Wrong!";
        }
    }

    public function unfriend($user_id, Request $request)
    {
        try {
            $this->userRepo->unfriendUser(Auth::user()->id, $user_id);

            // set unfriend status flash message to show in view
            $request->session()->flash('unfriend_status', true);
            return Redirect::to('home');

        } catch (\Exception $ex) {
            return "Something Went Wrong!";
        }
    }
}
