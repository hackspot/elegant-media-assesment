<?php

namespace App\Helpers;

use App\User;
use Illuminate\Support\Facades\Mail;


class MailHelper {

    /**
     *  Send Invitation Email to User
     * @param User $receiver
     * @param User $sender
     * @param String $invitation_code
     * @return boolean
     */
    public static function sendInvitationMail(User $receiver, User $sender, $invitation_code)
    {
        // Setup data as array
        $arrData = [
            'receiver' => [
                'first_name' => $receiver->first_name,
                'email' => $receiver->email,
            ],
            'sender' => [
                'first_name' => $sender->first_name,
                'email' => $sender->email,
            ],
            'invitation_code' => $invitation_code,
        ];

        /*
         * Trigger laravel's default email handler with data
         *  @return void
         */
        Mail::send('emails.invitation', $arrData, function($message) use ($arrData) {

            // set message receiver name and email
            $message->to($arrData['receiver']['email'], $arrData['receiver']['first_name'])

            // set message subject
            ->subject('Invitation From ' . $arrData['sender']['first_name'])

            // set message sender name and email
            ->from($arrData['sender']['email'],$arrData['sender']['first_name']);
        });

        return true;
    }
}