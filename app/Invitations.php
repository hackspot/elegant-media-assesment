<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invitations extends Model
{
    /**
     * Primary Key
     * @var string
     */
    protected $primaryKey = "_invitation_id";

    /**
     * Fillable Fields
     * @var array
     */
    protected $fillable = [
        'invitation_code', 'sent_from', 'sent_to'
    ];
}
