<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFriends extends Model
{
    /**
     * Primary Key
     * @var string
     */
    protected $primaryKey = "_pairId";

    /**
     * Fillable Fields
     * @var array
     */
    protected $fillable = [
        'pair_user_1', 'pair_user_2',
    ];
}
