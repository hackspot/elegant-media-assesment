<?php

namespace App\Repository;

use Illuminate\Support\Str;
use App\Repository\Contracts\UserRepositoryInterface;
use App\UserFriends;
use App\User;
use App\Invitations;
use App\Helpers\MailHelper;

class UserRepository implements UserRepositoryInterface {

    /**
     * Get friend list of a user by user's id.
     * -------------------------------------------
     * @param $userId
     * @param $searchKey
     * @return mix
     */
    public function getFriendsListByUserId(int $userId, string $searchKey)
    {

        // Select all friend id list of user and fetching the user data of those ids.
        $friendsList = User::select(
            'id',
            'first_name',
            'last_name',
            'email'
        )->whereIn(
            'id',
            UserFriends::select('pair_user_2')->where('pair_user_1', $userId)->get()->toArray()
        )

        // Filtering users by first name and last name with given searchKey.
        ->where(function ($query) use ($searchKey) {
            $query->where(
                'first_name',
                'like',
                '%'.$searchKey.'%'
            )->orwhere(
                'last_name',
                'like',
                '%'.$searchKey.'%'
            );
        })

        // Ordering final dataset in ascending by first name of user
        ->orderBy(
            'first_name', 'asc'
        )

        //Paginating results as 10 records per page
        ->paginate(10);

        return $friendsList;
    }

    /**
     * Get Non friend list of a user by user's id.
     * -------------------------------------------
     * @param $receiver_email
     * @param $sender
     * @return array
     */
    public function inviteUser(string $receiver_email, User $sender) {

        // find the user of given email address $receiver_email
        $receiverUser = User::where('email', $receiver_email)->first();

        // Checking whether user available or not
        if ($receiverUser) {

            // Check whether the user already friend or not
            $isAlreadyFriend = UserFriends::where([
                ['pair_user_1', '=', $sender->id],
                ['pair_user_2', '=', $receiverUser->id],
            ])->first();

            // return with message if already friend
            if ($isAlreadyFriend) {
                return [
                    'status' => false,
                    'message' => 'User is already in your friends list.'
                ];
            }

            // if not friend, Create friend invitation for each users
            $invitation = new Invitations();
            $invitation->invitation_code = Str::random(10);
            $invitation->sent_to = $receiverUser->id;
            $invitation->sent_from = $sender->id;
            $invitation->save();

            // send invitation to the user
            MailHelper::sendInvitationMail($receiverUser, $sender, $invitation->invitation_code);

            return [
                'status' => true,
                'message' => 'Invitation has been sent to '.$receiver_email,
            ];

        } else {

            // return with message, If user is not available
            return [
                'status' => false,
                'message' => 'User not found in system'
            ];
        }
    }

    /**
     * Accept Invitation
     * --------------------------------------------
     * @param $invitationCode
     * @param $userId
     * @return boolean
     */
    public function acceptInvitation(string $invitationCode, int $userId) {

        // Get invitation by invitation code and invited user id
        $invitation = Invitations::where([
            ['invitation_code', $invitationCode],
            ['sent_to', $userId]
        ])->first();

        // check whether invitation is available or not
        if ($invitation) {

            // Create Friend Record for receiver
            $friendPair = new UserFriends();
            $friendPair->pair_user_1 = $invitation->sent_to;
            $friendPair->pair_user_2 = $invitation->sent_from;
            $friendPair->save();

            unset($friendPair);

            // Create Friend Record for sender
            $friendPair = new UserFriends();
            $friendPair->pair_user_1 = $invitation->sent_from;
            $friendPair->pair_user_2 = $invitation->sent_to;
            $friendPair->save();

            // delete the invitation record
            $invitation->delete();
            return true;
        }

        return false;
    }

    /**
     * Unfriend User
     * --------------------------------------------
     * @param $loggedInUserId
     * @param $friendUserId
     */
    public function unfriendUser(int $loggedInUserId, int $friendUserId)
    {
        // get record or user pairs
        $pairCurrentUser = UserFriends::where([
            ['pair_user_1', $loggedInUserId],
            ['pair_user_2', $friendUserId]
        ])->first();

        $pairFriendUser = UserFriends::where([
            ['pair_user_2', $loggedInUserId],
            ['pair_user_1', $friendUserId]
        ])->first();

        // delete both records
        $pairCurrentUser->delete();
        $pairFriendUser->delete();

    }

}