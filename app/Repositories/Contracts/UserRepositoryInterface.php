<?php

namespace App\Repository\Contracts;

use App\User;

interface UserRepositoryInterface {

    public function getFriendsListByUserId(int $userId, string $searchKey);
    public function inviteUser(string $receiver_email, User $sender);
    public function acceptInvitation(string $invitationCode, int $userId);
    public function unfriendUser(int $loggedInUserId, int $friendUserId);
}
