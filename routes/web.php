<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home page route
Route::get('/', function () {
    return view('welcome');
});

// Laravel auth provider routes
Auth::routes(['verify' => true]);

// Secure route group with Auth & Verification Middleware
Route::middleware('verified')->group(function () {

    // Home Screen View
    Route::get('/home', 'HomeController@friendsListView')->name('home');

    // Invite Screen View
    Route::get('/invite', 'HomeController@inviteView')->name('invite');

    // Send Invitation to user
    Route::post('/invite', 'InvitationController@sendInvitation')->name('sendInvitation');

    // Accept Invitation from mail
    Route::get('/invite/{invitation_code}', 'InvitationController@acceptInvitation')->name('acceptInvitation');

    // Unfriend a user from list
    Route::get('/unfriend/{user_id}', 'HomeController@unfriend')->name('unfriend');
});
