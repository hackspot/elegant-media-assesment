@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Friends List</div>
                <div class="card-body">
                    <!-- Tabs Navigator -->
                    <div>
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                @if (Route::has('home'))
                                    <a class="nav-link @if($tab === 'friendsList') active @endif" href="{{ route('home') }}">
                                        My List
                                    </a>
                                @endif
                            </li>
                            <li class="nav-item">
                                @if (Route::has('invite'))
                                    <a class="nav-link @if($tab === 'invite') active @endif" href="{{ route('invite') }}">
                                        Invite Friend
                                    </a>
                                @endif
                            </li>
                        </ul>
                    </div>
                    <!-- /Tabs Navigator -->
                    <!-- Tabs Container -->

                    @if($tab === 'friendsList')

                        <div class="pt-2">
                            @if($accept_status = Session::get('accept_status'))
                                @if($accept_status['status'])
                                    <div class="alert alert-success">{{ $accept_status['message'] }}</div>
                                @else
                                    <div class="alert alert-danger">{{ $accept_status['message'] }}</div>
                                @endif
                            @endif

                            @if(Session::get('unfriend_status'))
                                <div class="alert alert-info">User has been removed from your friends list.</div>
                            @endif

                            @if(count($friendsList) > 0)

                                    <form>
                                        <div class="form-group row pt-4">
                                            <label for="name" class="col-md-12 col-form-label">{{ __('Filter by name') }}</label>
                                            <div class="col-sm-6">
                                                <input id="name" type="name" class="form-control" name="name" value="{{ request()->input('name') }}" autocomplete="email" autofocus>
                                                <input type="hidden" name="page" value="{{ $friendsList->currentPage() }}">
                                            </div>
                                            <div class="col-sm-6">
                                                <button type="submit" class="btn btn-primary">Search</button>
                                            </div>
                                        </div>
                                    </form>

                                <ul class="list-group">
                                @foreach($friendsList as $friend)
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-sm-10">
                                                <div><strong>{{ $friend->first_name }} {{ $friend->last_name }}</strong></div>
                                                <div>{{ $friend->email }}</div>
                                            </div>
                                            <div class="col-sm-2">
                                                <a href="{{ route('unfriend', ['user_id' => $friend->id]) }}">
                                                    <button class="btn btn-danger">Unfriend</button>
                                                </a>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                                </ul>
                                <div class="pt-2">
                                    Showing {{($friendsList->currentpage()-1)*$friendsList->perpage()+1}} to {{$friendsList->currentpage()*$friendsList->perpage()}}
                                    of  {{$friendsList->total()}} entries
                                </div>
                                <div class="pt-2">
                                    {{ $friendsList->appends(request()->input())->links() }}
                                </div>
                            @else
                                Your friends list is empty!
                            @endif
                        </div>

                    @elseif($tab === 'invite')

                        <form action="{{ route('sendInvitation') }}" method="post">
                            @csrf
                            <div class="form-group row pt-4">
                                <label for="email" class="col-md-12 col-form-label">{{ __('E-Mail Address') }}</label>
                                <div class="col-sm-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                </div>
                                <div class="col-sm-6">
                                    <button type="submit" class="btn btn-primary">Invite</button>
                                </div>
                            </div>
                        </form>

                        @if($invitation_status = Session::get('invitation_status'))
                            @if($invitation_status['status'])
                                <div class="alert alert-success">{{ $invitation_status['message'] }}</div>
                            @else
                                <div class="alert alert-danger">{{ $invitation_status['message'] }}</div>
                            @endif
                        @endif

                        @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                    @endif
                    <!-- /Tabs Container -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
